import React from "react"
import dragon from './images/dragon.jpg'
import detonator from './images/detonator.jpg'
import jammer from './images/jammer.jpg'
import spacialagent from './images/spacialagent.jpg'
import explosives from './images/explosives.jpg'
import explosiveexpert from './images/explosiveexpert.jpg'
import { Bonus, Item } from './autostuff'
import { getParent, getSiblings } from './itemTreeCrawler'
/*
 export function pickNextCard(previousCard, state) {
 return cards.explosives.id;
 }*/


export function getCardById(name) {
  if (name && name.indexOf(':') > -1) {
    const [fnName, ...params] = name.split(':')
    return dynamicCards[fnName](...params)
  }
  return Object.values(cards).find(c => c.id === name)
}

export function itemHasBonus(item, bonus) {
  const card = getCardById(item)
  return card != null && card.bonusList != null && card.bonusList.some(b => b.bonusId === bonus.bonusId)
}

export function stationHasEffect(powerstation, attrName) {
  return powerstation != null && powerstation.calculatedAttributes != null && powerstation.calculatedAttributes[attrName] > 0
}

export function calculateOutput(calculatedAttributes) {
  return Math.max(0, calculatedAttributes.manpower) * Math.max(0, calculatedAttributes.efficiency)
}

export function calculateAllOutputs(stations) {
  let count = 0
  Object.values(stations).forEach(s => {
    count += (calculateOutput(s.calculatedAttributes || s.attributes))
  })
  return count
}
export function calculateMaxOutput(stations) {
  let count = 0
  Object.values(stations).forEach(s => {
    count += (calculateOutput(s.attributes))
  })
  return count
}


export const bonuses = {
  manpowerplus1                        : new Bonus('manpower +1', () => ({ manpower : 1 }), '+1 manpower'),
  manpowerminus1                       : new Bonus('manpower -1', () => ({ manpower : -1 }), '-1 manpower'),
  efficiencyminus10                    : new Bonus('efficiency -10', () => ({ efficiency : -10 }), '-10 efficiency'),
  efficiencyminus5perinfiltratedworker : new Bonus(
    'efficiency -5 per infiltrated worker',
    (attr, item, rootItems) => {
      const siblings = getSiblings(rootItems, item.id)
      if (Array.isArray(siblings)) {
        return { efficiency : -5 * siblings.filter(sib => sib.id === cards.infiltratedWorker.id).length }
      }
      return { efficiency : 0 }
    },
    (calculatedValue, attr, item, rootItems) => {
      return `(${calculatedValue.efficiency}) -5 efficiency per infiltrates worker`
    }),

  jammed_efficiencyminus5perinfiltratedworker : new Bonus(
    '[Jammed] efficiency -5 per infiltrated worker',
    (attr, item, rootItems) => {
      if (!attr.jammed) return { efficiency : 0 }
      const siblings = getSiblings(rootItems, item.id)
      if (Array.isArray(siblings)) {
        return { efficiency : -5 * siblings.filter(sib => sib.id === cards.infiltratedWorker.id).length }
      }
      return { efficiency : 0 }
    },
    (calculatedValue, attr, item, rootItems) => {
      if (!attr.jammed) return '(inactive) [Jammed] -5 efficiency per infiltrated worker'
      return `(${calculatedValue.efficiency}) [Jammed] -5 efficiency per infiltrates worker`
    }),

  backdoor_manpowerminus5 : new Bonus(
    '[Backdoor] manpower -5',
    (attr, item, rootItems) => {
      if (!attr.backdoor) return { manpower : 0 }
      return { manpower : -5 }
    },
    (calculatedValue, attr, item, rootItems) => {
      if (!attr.backdoor) return '(inactive) [Backdoor] -5 manpower'
      return `(${calculatedValue.manpower}) [Backdoor] -5 manpower`
    }),

  jammed_manpowerminus2 : new Bonus(
    '[Jammed] manpower -2',
    (attr, item, rootItems) => {
      if (!attr.jammed) return { manpower : 0 }
      return { manpower : -2 }
    },
    (calculatedValue, attr, item, rootItems) => {
      if (!attr.jammed) return '(inactive) [Jammed] -2 manpower'
      return `(${calculatedValue.manpower}) [Jammed] -2 manpower`
    }),

  activate_removeJammer         : new Bonus(
    'when placed, remove all Jammer from the Power Station',
    (attr, item, rootItems) => ({}),
    null
  ),
  activate_backdoor_addSabotage : new Bonus(
    '[Backdoor] when placed, put an additional "Explosives" card',
    (attr, item, rootItems) => ({}),
    null
  ),
  activate_jammed_noeffect      : new Bonus(
    '[Jammed] When placed, Discard the detonator. The detonator has not effect.',
    (attr, item, rootItems) => ({}),
    null
  ),

  activate_remove_saboted_infiltrated : new Bonus(
    'When placed, remove all card providing "sabotaged" and all Infiltrated Workers.',
    (attr, item, rootItems) => ({}),
    null
  ),
  manpowerminus15perremove_fake       : new Bonus(
    `-15 manpower per card removed`,
    (attr, item, rootItems) => {
      return {}
    },
    (calculatedValue, attr, item, rootItems) => {
      return null
    }),


  backdoor  : new Bonus(
    'provide "Backdoor"',
    () => ({ backdoor : 1 }),
    'provide "Backdoor"',
    -1
  ),
  jammed    : new Bonus(
    'provide "Jammed"',
    () => ({ jammed : 1 }),
    'provide "Jammed"',
    -1
  ),
  sabotaged : new Bonus(
    'provide "sabotaged"',
    () => ({ sabotaged : 1 }),
    'provide "sabotaged"',
    -1
  ),

  backdoor_sabotaged : new Bonus(
    '[Backdoor] provide "sabotaged"',
    (attr, item, rootItems) => {
      if (!attr.backdoor) return { manpower : 0 }
      return { manpower : -5 }
    },
    (calculatedValue, attr, item, rootItems) => {
      if (!attr.backdoor) return '(inactive) [Backdoor] provide "sabotaged"'
      return `[Backdoor] provide "sabotaged"`
    }),
}

export const cards = {
  infiltratedWorker : {
    img   : dragon,
    quote : "The man is a worker.",
    ...Item(
      'Infiltrated Worker',
      [
        bonuses.manpowerplus1,
        bonuses.efficiencyminus5perinfiltratedworker,
        bonuses.jammed_efficiencyminus5perinfiltratedworker,
        bonuses.backdoor,
      ]
    )
  },
  jammer            : {
    img   : jammer,
    quote : "I d…t und…t…d. Co…d you re…t ?",
    ...Item(
      'Jammer',
      [
        bonuses.efficiencyminus10,
        bonuses.jammed,
      ]
    )
  },
  specialAgent      : {
    img   : spacialagent,
    quote : "My name is Bond. Fury Bond.",
    ...Item(
      'Special Agent',
      [
        bonuses.backdoor_manpowerminus5,
        bonuses.jammed_manpowerminus2,
        bonuses.manpowerminus1,
      ]
    )
  },
  explosiveExpert   : {
    img   : explosiveexpert,
    quote : "My name is Bomb.",
    ...Item(
      'Explosive expert',
      [
        bonuses.activate_removeJammer,
        bonuses.sabotaged,
      ]
    )
  },
  explosives        : {
    img   : explosives,
    quote : "Shhhhhhhhhh…",
    ...Item(
      'Explosives',
      [
        bonuses.sabotaged,
        bonuses.activate_backdoor_addSabotage
      ]
    )
  },
  detonator         : {
    img   : detonator,
    quote : "…BOOM!",
    ...Item(
      'Detonator',
      [
        bonuses.activate_jammed_noeffect,
        bonuses.activate_remove_saboted_infiltrated,
        bonuses.manpowerminus15perremove_fake,
      ]
    )
  },
}

export const dynamicCards = {
  detonator : (removed) => {
    return {
      ...cards.detonator,
      bonusList : [
        new Bonus(
          `-5 manpower per card removed`,
          (attr, item, rootItems) => {
            return { manpower : -15 * removed }
          },
          (calculatedValue, attr, item, rootItems) => {
            return `(${-15 * removed}) manpower per card removed (-15×${removed})`
          })
      ]
    }
  }
}


const deck = []

function shuffle(a) {
  for (let i = a.length; i; i--) {
    let j = Math.floor(Math.random() * i);
    [a[i - 1], a[j]] = [a[j], a[i - 1]];
  }
}
function refill() {
  deck.push(...Array(3).fill(cards.infiltratedWorker.id))
  deck.push(...Array(2).fill(cards.specialAgent.id))
  deck.push(...Array(2).fill(cards.explosives.id))
  deck.push(...Array(1).fill(cards.explosiveExpert.id))
  deck.push(...Array(2).fill(cards.jammer.id))
  deck.push(...Array(1).fill(cards.detonator.id))
  shuffle(deck)
}
deck.push(...Array(1).fill(cards.infiltratedWorker.id))
deck.push(...Array(1).fill(cards.explosives.id))
deck.push(...Array(1).fill(cards.specialAgent.id))
deck.push(...Array(1).fill(cards.jammer.id))
deck.push(...Array(1).fill(cards.infiltratedWorker.id))

export function pickNextCard(previousCard, state) {
  if (deck.length === 0) refill()
  return deck.pop();
}