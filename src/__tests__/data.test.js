import { cards } from '../data'
import { calculate } from '../autostuff'

function getDescriptions(calculatedItems) {
  return calculatedItems
    .map(item => (
      '---\n' +
      'Item ' + item.id + '\n' +
      item.bonusList
        .map(b => b.dynamicDescription)
        .join('\n')
    ))
    .join('\n')
}

const powerStationAttrs = {
  manpower   : 100,
  efficiency : 100,
}

it('calculates infiltratedWorker', () => {
  let calculated = calculate(powerStationAttrs, [cards.infiltratedWorker])
  expect(calculated.calculatedAttributes).toMatchSnapshot();
  expect(getDescriptions(calculated.calculatedItems)).toMatchSnapshot();

  let calculated2 = calculate(powerStationAttrs, [cards.infiltratedWorker, cards.infiltratedWorker])
  expect(calculated2.calculatedAttributes).toMatchSnapshot();
  expect(getDescriptions(calculated2.calculatedItems)).toMatchSnapshot();

  let calculated3 = calculate(powerStationAttrs, [cards.infiltratedWorker, cards.infiltratedWorker, cards.jammer])
  expect(calculated3.calculatedAttributes).toMatchSnapshot();
  expect(getDescriptions(calculated3.calculatedItems)).toMatchSnapshot();

});

it('calculates jammer', () => {
  let calculated3 = calculate(powerStationAttrs, [cards.jammer])
  expect(calculated3.calculatedAttributes).toMatchSnapshot();
  expect(getDescriptions(calculated3.calculatedItems)).toMatchSnapshot();
})

it('calculates SpecialAgent', () => {
  let calculated1 = calculate(powerStationAttrs, [cards.specialAgent])
  expect(calculated1.calculatedAttributes).toMatchSnapshot();
  expect(calculated1.calculatedAttributes.manpower).toEqual(99);
  expect(getDescriptions(calculated1.calculatedItems)).toMatchSnapshot();

  let calculated2 = calculate(powerStationAttrs, [cards.specialAgent, cards.jammer])
  expect(calculated2.calculatedAttributes).toMatchSnapshot();
  expect(calculated2.calculatedAttributes.manpower).toEqual(97);
  expect(getDescriptions(calculated2.calculatedItems)).toMatchSnapshot();

  let calculated3 = calculate(powerStationAttrs, [cards.specialAgent, cards.infiltratedWorker])
  expect(calculated3.calculatedAttributes).toMatchSnapshot();
  expect(calculated3.calculatedAttributes.manpower).toEqual(100 - 5 - 1 + 1);
  expect(getDescriptions(calculated3.calculatedItems)).toMatchSnapshot();

  let calculated4 = calculate(powerStationAttrs, [cards.specialAgent, cards.infiltratedWorker, cards.jammer])
  expect(calculated4.calculatedAttributes).toMatchSnapshot();
  expect(getDescriptions(calculated4.calculatedItems)).toMatchSnapshot();
})