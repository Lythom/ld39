import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';
import { Item } from '../autostuff'

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

it('can add item', () => {
  const app = new App();
  app.setState = jest.fn()
  const item = 'Jammer'
  const item2 = 'Jammer'

  expect(app.state.powerStations['Power Station 1'].calculatedItems).toBe(undefined)
  expect(app.state.powerStations['Power Station 1'].calculatedAttributes).toBe(undefined)

  app.actions.addItem('Power Station 1', item);
  app.state = app.setState.mock.calls[0][0]

  expect(app.state.powerStations['Power Station 1'].items[0]).toEqual(item)
  expect(app.state.powerStations['Power Station 2']).toBeTruthy()
  expect(app.state.powerStations['Power Station 1'].calculatedItems).toBeTruthy()
  expect(app.state.powerStations['Power Station 1'].calculatedAttributes).toBeTruthy()

  app.actions.addItem('Power Station 1', item2)
  app.state = app.setState.mock.calls[1][0]
  expect(app.state.powerStations['Power Station 1'].items[1]).toEqual(item2)
  expect(app.state.powerStations['Power Station 1'].calculatedItems).toBeTruthy()
  expect(app.state.powerStations['Power Station 1'].calculatedAttributes).toBeTruthy()
});


it('can filter item', () => {
  // items are named and must exist in data.js
  const app = new App();
  app.setState = jest.fn()
  const item = 'Explosive expert'
  const item2 = 'Special Agent'
  const item3 = 'Jammer'
  app.actions.addItem('Power Station 1', item)
  app.state = app.setState.mock.calls[0][0]
  app.actions.addItem('Power Station 1', item2)
  app.state = app.setState.mock.calls[1][0]
  app.actions.addItem('Power Station 1', item3)
  app.state = app.setState.mock.calls[2][0]
  expect(app.state.powerStations['Power Station 1'].items.length).toBe(3)

  app.state = app.actions.filterItem(app.state, 'Power Station 1', i => i.indexOf('Agent') === -1)
  expect(app.state.powerStations['Power Station 1'].items.length).toBe(2)
});
