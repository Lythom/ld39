import {Item, Bonus, calculate, applyBonusValue, applyValue} from '../autostuff'
import {getParent, getSiblings} from '../itemTreeCrawler'

const baseAttributes = {
    shape   : [{"x" : -0.0032599639892577637, "y" : -0.9259200000762939}, {"x" : 0.20230270385742188, "y" : -0.9147940921783447}, {
        "x" : 0.40531768798828116,
        "y" : -0.8602136611938477
    }, {"x" : 0.6035221862792968, "y" : -0.7552891540527343}, {"x" : 0.754704132080078, "y" : -0.6144544601440429}, {
        "x" : 0.8733609008789063,
        "y" : -0.4259569549560547
    }, {"x" : 0.9475004577636719, "y" : -0.21517112731933596}, {"x" : 0.9706768798828125, "y" : -0.008074417114257826}, {
        "x" : 0.9451837158203125,
        "y" : 0.21367553710937504
    }, {"x" : 0.8754989624023437, "y" : 0.41006546020507817}],
    range   : 100,
    damage  : 10,
    effects : []
}

const bonuses = {
    dmgFlatBonus        : Bonus('Add 5 damage', attr => ({damage : 5}), "Add 5 damage", 0),
    dmgMultBonus        : Bonus('Add 10% damage + bonus', attr => ({damage : attr.damage * 0.1}), 'Add 10% damage + bonus', 1),
    dmgMultBaseBonus    : Bonus('Add 10% base damage', attr => ({damage : attr.damage * 0.1}), 'Add 10% base damage', 0),
    dmgFromRangeBonus   : Bonus('Add 5% range as damage', attr => ({damage : attr.range * 0.05}), 'Add 5% range as damage', 1),
    rangeAndDamageBonus : Bonus('Add 2 range and 2 damage', attr => ({damage : 2, range : 2}), 'Add 2 range and 2 damage', 0),
}

const expected = {
    dmgFlatBonus        : Object.assign({}, baseAttributes, {damage : 15}),
    dmgMultBonus        : Object.assign({}, baseAttributes, {damage : 11}),
    dmgMultBaseBonus    : Object.assign({}, baseAttributes, {damage : 11}),
    dmgFromRangeBonus   : Object.assign({}, baseAttributes, {damage : 15}),
    rangeAndDamageBonus : Object.assign({}, baseAttributes, {damage : 12, range : 102}),
}


/**
 * Rich test.
 */
const getHolderParentSiblingItems = function(rootItems, itemId) {
    const parent = getParent(rootItems, itemId)
    return getSiblings(rootItems, parent.id)
}

const swordBooster = Item("SwordBooster", [Bonus('Add 15 range', (attr, bonus) => ({range : 15}), 'Add 15 range', 0)])

const shieldSynergyForSword = Item("ShieldSynergyForSword", [
    Bonus('[with a shield][on a sword] +10% range',
        (attr, item, rootItems) => {
            const parentSiblingItems = getHolderParentSiblingItems(rootItems, item.id)
            const parentItem = getParent(rootItems, item.id)
            if (Array.isArray(parentSiblingItems) && parentSiblingItems.some(item => item.type === "shield") && parentItem != null && parentItem.type === 'sword') {
                return {range : attr.range * 0.1}
            }
            return {}
        },
        (calculatedValue, attr, item, rootItems) => {
            let description = ' +10% range'
            const parentItem = getParent(rootItems, item.id)
            if (parentItem === null || parentItem.type !== 'sword') {
                description = '[must be on a sword !]' + description
            } else {
                description = '[on a sword]' + description
            }
            const parentSiblingItems = getHolderParentSiblingItems(rootItems, item.id)
            if (!Array.isArray(parentSiblingItems) || !parentSiblingItems.some(item => item.type === "shield")) {
                description = '[A shield is required for the synergy]' + description
            } else {
                description = '[with a shield]' + description
            }
            return description
        },
        1)
])

const shield = Item('Shield1')
shield.type = 'shield'

const sword = Item("Sword1", [
    Bonus('+5 range', (attr, bonus) => ({range : 5}), '+5 range', 0)
], [
    swordBooster,
    shieldSynergyForSword
])
sword.type = 'sword'

describe('autostuff', function() {

    it('No equipment => return initial attributes', function() {
        const result = calculate(baseAttributes, null)
        expect(result.calculatedAttributes).toEqual(baseAttributes)
    })

    it('equip a stuff with one bonus => return new object with calculated attributes', function() {
        // tests with one bonus
        Object.keys(bonuses).forEach(bonusKey => {
            let result = calculate(baseAttributes, [Item(bonusKey, [bonuses[bonusKey]])])
            expect(result.calculatedAttributes).toEqual(expected[bonusKey])
        })

        // tests with priority
        let result = calculate(baseAttributes, [Item('Test combo 1', [bonuses.dmgFlatBonus, bonuses.dmgMultBonus])])
        // check the result
        expect(result.calculatedAttributes).toEqual(Object.assign({}, baseAttributes, {damage : 16.5}))
        // check the calculation report
        expect(result.calculatedItems[0].bonusList[0].calculatedValue).toEqual({damage : 5})
        expect(result.calculatedItems[0].bonusList[1].calculatedValue).toEqual({damage : 1.5})

        result = calculate(baseAttributes, [Item('Test combo 2', [bonuses.dmgFlatBonus, bonuses.dmgMultBaseBonus])])
        // check the result
        expect(result.calculatedAttributes).toEqual(Object.assign({}, baseAttributes, {damage : 16}))
        // check the calculation report
        expect(result.calculatedItems[0].bonusList[0].calculatedValue).toEqual({damage : 5})
        expect(result.calculatedItems[0].bonusList[1].calculatedValue).toEqual({damage : 1})

        // tests with subitems
        let subItem1 = Item('+10% atk modifier', [bonuses.dmgMultBonus])
        result = calculate(baseAttributes, [Item('I\'ll equip a modifier', [bonuses.dmgFlatBonus], [subItem1])])
        // check the result
        expect(result.calculatedAttributes).toEqual(Object.assign({}, baseAttributes, {damage : 15.5})) // 10 (base) + 5 (item) + 0.5 (subitem)
        // check the calculation report
        expect(result.calculatedItems[0].bonusList[0].calculatedValue).toEqual({damage : 5})
        expect(result.calculatedItems[0].equipedItems[0].bonusList[0].calculatedValue).toEqual({damage : 0.5})
    })

    it('Equip items that have subitems that have subitem, with special bonus calculation', function() {
        // 2 items and one subitem
        let result = calculate(baseAttributes, [sword, shield])
        expect(result.calculatedItems[0].bonusList[0].calculatedValue).toEqual({range : 5}) // Sword itslefs Add 5 range
        expect(result.calculatedItems[0].equipedItems[0].bonusList[0].calculatedValue).toEqual({range : 15}) // swordBooster Add 15 range
        expect(result.calculatedItems[0].equipedItems[1].bonusList[0].calculatedValue).toEqual({range : 2}) // ShieldSynergyForSword condidional bonus
        expect(result.calculatedItems[0].equipedItems[1].bonusList[0].dynamicDescription).toEqual('[with a shield][on a sword] +10% range')
        expect(result.calculatedItems[1].bonusList.length).toEqual(0) // shield gives no bonus
        expect(result.calculatedAttributes).toEqual(Object.assign({}, baseAttributes, {range : 122}))

        result = calculate(baseAttributes, [sword])
        expect(result.calculatedItems[0].bonusList[0].calculatedValue).toEqual({range : 5}) // Sword itslefs Add 5 range
        expect(result.calculatedItems[0].equipedItems[0].bonusList[0].calculatedValue).toEqual({range : 15}) // swordBooster Add 15 range
        expect(result.calculatedItems[0].equipedItems[1].bonusList[0].calculatedValue).toEqual({}) // ShieldSynergyForSword condidional bonus
        expect(result.calculatedItems[0].equipedItems[1].bonusList[0].dynamicDescription).toEqual('[A shield is required for the synergy][on a sword] +10% range')
        expect(result.calculatedAttributes).toEqual(Object.assign({}, baseAttributes, {range : 120}))

        // full test of everything
    })

    it('applyBonusValue applies a bonusValue object on an attributeSet', function() {
        // test with integers
        expect(applyBonusValue({a : 1}, {a : 2})).toEqual({a : 3})
        expect(applyBonusValue({a : 1}, {b : 2})).toEqual({a : 1, b : 2})

        // test with objects
        expect(applyBonusValue({a : {c : 1}}, {a : {c : 2}})).toEqual({a : {c : 3}})
        expect(applyBonusValue({a : {c : 1}}, {a : {d : 2}})).toEqual({a : {c : 1, d : 2}})

        // test with array
        expect(applyBonusValue({a : [1]}, {a : [2]})).toEqual({a : [3]})
        expect(applyBonusValue({a : ['r']}, {a : ['t']})).toEqual({a : ['rt']})
        expect(applyBonusValue({a : ['t']}, {a : ['r']})).toEqual({a : ['tr']})
        expect(applyBonusValue({a : [1]}, {a : [null, 2]})).toEqual({a : [1, 2]})
        expect(applyBonusValue({a : [1]}, {b : [5]})).toEqual({a : [1], b : [5]})
        expect(applyBonusValue({a : [1]}, {a : [5]})).toEqual({a : [6]})
        expect(applyBonusValue({a : {c : 1}}, {a : {c : 2}})).toEqual({a : {c : 3}})
        expect(applyBonusValue({a : {c : 1}}, {a : {d : 2}})).toEqual({a : {c : 1, d : 2}})

        // error cases
        let test = null
        try { test = applyBonusValue(1, 2) } catch (e) {}
        expect(test).toEqual(null)
        try { test = applyBonusValue([3], 4) } catch (e) {}
        expect(test).toEqual(null)
        try { test = applyBonusValue(5, [6]) } catch (e) {}
        expect(test).toEqual(null)
        try { test = applyBonusValue({a : 7}, 8) } catch (e) {}
        expect(test).toEqual(null)
        try { test = applyBonusValue(9, {a : 10}) } catch (e) {}
        expect(test).toEqual(null)
        try { test = applyBonusValue(1, true) } catch (e) {}
        expect(test).toEqual(null)
        try { test = applyBonusValue(true, true) } catch (e) {}
        expect(test).toEqual(null)
        try { test = applyBonusValue({a : true}, {a : true}) } catch (e) {}
        expect(test).toEqual(null)
    })

    it('applyValue applies a bonus value on an attribute value', function() {

            // use case
            expect(applyValue(1, 2, 'add')).toEqual(3)
            expect(applyValue(2, 1, 'add')).toEqual(3)
            expect(applyValue(null, -2, 'add')).toEqual(-2)
            expect(applyValue(-2, null, 'add')).toEqual(-2)
            expect(applyValue(undefined, -3, 'add')).toEqual(-3)
            expect(applyValue(-3, undefined, 'add')).toEqual(-3)
            expect(applyValue(1, null, 'add')).toEqual(1)
            expect(applyValue(null, 1, 'add')).toEqual(1)
            expect(applyValue(4, NaN, 'add')).toEqual(4)
            expect(applyValue(NaN, 4, 'add')).toEqual(4)
            expect(applyValue(true, false, 'add')).toEqual(1)
            expect(applyValue(false, true, 'add')).toEqual(1)
            expect(applyValue(true, true, 'add')).toEqual(2)
            expect(applyValue(false, false, 'add')).toEqual(0)

            expect(applyValue({a : 1}, {a : 2}, 'merge')).toEqual({a : 3})
            expect(applyValue({a : 2}, {a : 1}, 'merge')).toEqual({a : 3})
            expect(applyValue([1, 2], [3, 4], 'merge')).toEqual([4, 6])
            expect(applyValue([3, 4], [1, 2], 'merge')).toEqual([4, 6])

            // error cases
            let test = null

            try { test = applyValue(1, 2, 'anoperationthatdonotexists') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue([3], 4, 'anoperationthatdonotexists') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue(5, [6], 'anoperationthatdonotexists') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue({a : 7}, 8, 'anoperationthatdonotexists')} catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue(9, {a : 10}, 'anoperationthatdonotexists') } catch (e) {}
            expect(test).toEqual(null)

            try { test = applyValue(1, 2, 'merge') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue(3, 4, 'append') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue(5, 6, 'prepend') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue(7, [8], 'merge') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue(9, [10], 'append') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue(11, [12], 'prepend') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue([13], 14, 'merge') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue([15], 16, 'append') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue([17], 18, 'prepend') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue(7, {a : 18}, 'merge') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue(9, {a : 110}, 'append') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue(11, {a : 112}, 'prepend') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue({a : 113}, 14, 'merge') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue({a : 115}, 16, 'append') } catch (e) {}
            expect(test).toEqual(null)
            try { test = applyValue({a : 117}, 18, 'prepend') } catch (e) {}
            expect(test).toEqual(null)

        }
    )


})
