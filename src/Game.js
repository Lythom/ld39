import React, { Component } from 'react';
import './App.css';
import Background from './Background'

import Cards from './Cards'
import PowerStations from './PowerStations'
import { FlatButton } from 'material-ui'
import { calculateAllOutputs } from './data'
import { maxOutput } from './App'

class Game extends Component {

  componentDidMount() {
    this.props.actions.pickNewCard()
  }

  render() {
    return (
      <div className="Game">
        <FlatButton onClick={this.props.actions.reset} className="bordered" style={{ position : 'absolute', top : 675, right : 5, color : '#8E312A', fontWeight: "bold", padding: '2px 5px' }}>
          To mission screen
        </FlatButton>
        <Background game={this.props.game} actions={this.props.actions} />
        <PowerStations game={this.props.game} actions={this.props.actions} />
        <div className="abs"style={{ color: 'white', transform : `translate(725px, 105px)`}}>
          <small>POWERING</small><br/>
          <strong>{Math.round(this.props.game.cumulatedEnergy * 10000 / this.props.game.energyBeforeLose)/100} %</strong> (+ {Math.round(calculateAllOutputs(this.props.game.powerStations) * 1000 / this.props.game.energyBeforeLose) / 10} %)<br/><br/>
          <small>SABOTAGE PROGRESS</small><br/>
          <strong>{Math.round((maxOutput - calculateAllOutputs(this.props.game.powerStations)) * 10000 / maxOutput)/100} %</strong>
        </div>
        <Cards game={this.props.game} actions={this.props.actions} />
      </div>
    );
  }
}

export default Game;
