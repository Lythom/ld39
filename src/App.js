import React, { Component } from 'react';
import './App.css';
import Intro from './Intro'
import Game from './Game'
import update from 'immutability-helper';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { pickNextCard, getCardById, calculateAllOutputs, calculateMaxOutput, bonuses, itemHasBonus, cards, stationHasEffect } from './data'

import powerstation1 from './images/powerstation1.png'
import powerstation2 from './images/powerstation2.png'
import powerstation3 from './images/powerstation3.png'
import { calculate } from './autostuff'
import { Dialog, FlatButton } from 'material-ui'

export const initialState = {
  screen           : 'intro',
  speed            : 1,
  cumulatedEnergy  : 0,
  energyBeforeLose : 1200000,
  powerStations    : {
    'Power Station 1' : {
      attributes : {
        manpower   : 100,
        efficiency : 100,
      },
      items      : [],
      img        : powerstation1
    },
    'Power Station 2' : {
      attributes : {
        manpower   : 100,
        efficiency : 200,
      },
      items      : [],
      img        : powerstation2
    },
    'Power Station 3' : {
      attributes : {
        manpower   : 200,
        efficiency : 80,
      },
      items      : [],
      img        : powerstation3
    }
  },
  currentCard      : null,
}

export const maxOutput = calculateMaxOutput(initialState.powerStations)

class App extends Component {

  constructor() {
    super()
    this.gotoIntro = this.gotoIntro.bind(this)

    this.state = initialState

    this.actions = {
      goto           : (screen) => {
        this.setState({ screen })
      },
      pickNewCard    : () => {
        this.setState(prevState => ({
          currentCard : pickNextCard(prevState.currentCard, prevState)
        }))
      },
      addItem        : (station, item) => {
        let nextState = this.actions.pushItem(this.state, station, item)
        nextState = this.actions.updateStation(nextState, station)
        nextState = this.actions.triggerEffects(nextState, item, station)
        this.setState(nextState)
      },
      pushItem       : (state, station, item) => {
        return update(state, {
          powerStations : { [station] : { items : { $push : [item] } } },
        })
      },
      filterItem     : (state, station, filterFn) => {
        let nextState = update(state, {
          powerStations : { [station] : { items : { $set : state.powerStations[station].items.filter(filterFn) } } }
        })
        nextState = this.actions.updateStation(nextState, station)
        return nextState
      },
      triggerEffects : (state, item, station) => {
        let nextState = state
        if (itemHasBonus(item, bonuses.activate_removeJammer)) {
          nextState = this.actions.filterItem(nextState, station, item => item !== cards.jammer.id)
        }
        if (itemHasBonus(item, bonuses.activate_backdoor_addSabotage) && stationHasEffect(nextState.powerStations[station], 'backdoor')) {
          nextState = this.actions.pushItem(nextState, station, item)
        }
        if (itemHasBonus(item, bonuses.activate_jammed_noeffect)) {
          if (stationHasEffect(nextState.powerStations[station], 'jammed')) {
            // remove last, no other effect
            nextState = this.actions.filterItem(nextState, station, (item, idx) => idx !== nextState.powerStations[station].items.length - 1)
          } else if (itemHasBonus(item, bonuses.activate_remove_saboted_infiltrated)) {
            // replace last with value detonator
            nextState = this.actions.filterItem(nextState, station, (item, idx) => idx !== nextState.powerStations[station].items.length - 1)
            const isSabotagedOrInfiltrated = i => {
              return i.id === cards.infiltratedWorker.id || i.bonusList.some(b => b.calculatedValue.sabotaged > 0)
            }
            // count removed
            const toRemove = nextState.powerStations[station].calculatedItems.filter(isSabotagedOrInfiltrated).map(i => i.id)
            const count = toRemove.length
            // do remove
            nextState = this.actions.filterItem(nextState, station, itemName => toRemove.indexOf(itemName) === -1)
            // push new detonator
            nextState = this.actions.pushItem(nextState, station, "detonator:" + count)
          }
        }

        if (nextState !== state) {
          nextState = this.actions.updateStation(nextState, station)
        }
        return nextState
      },

      updateStation : (state, station) => {
        const calculated = calculate(state.powerStations[station].attributes, state.powerStations[station].items.map(getCardById))
        let nextState = update(state, {
          powerStations : {
            [station] : {
              $merge : calculated
            },
          }
        })
        const outputs = calculateAllOutputs(nextState.powerStations)
        nextState = update(nextState, {
          speed           : { $set : Math.max(0, outputs / maxOutput) },
          cumulatedEnergy : { $set : nextState.cumulatedEnergy += outputs }
        })
        return nextState
      },

      getCurrentCards : () => ([
        getCardById(this.state.currentCard)
      ]),

      reset : (e) => {
        e && e.preventDefault()
        e && e.stopPropagation()
        localStorage.removeItem('state');
        this.setState(initialState);
      }
    }
  }

  componentDidMount() {
    window.addEventListener('keydown', this.gotoIntro)
    let state = null;
    try {
      state = JSON.parse(localStorage.getItem('state'))
    } catch (e) {}
    if (state) {
      this.setState(state)
    }
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.gotoIntro)
    localStorage.setItem('state', JSON.stringify(this.state));
  }

  gotoIntro = (e) => {
    if (e.which === 27) {
      this.actions.reset()
    }
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
        <div className="App">
          <Dialog
            actions={[
              <FlatButton onClick={this.actions.reset} style={{ color : '#CCC', fontWeight : "bold", padding : '2px 5px' }}>Back to mission screen</FlatButton>
            ]}
            modal={false}
            open={this.state.speed === 0}
            onRequestClose={this.actions.reset}
            style={{ textAlign : 'center' }}
          >
            <div style={{ color : 'white' }}>
              <strong>The Star Killer went out of energy. <br/>Sabotage complete ! </strong><br/><br/>
              Space is safe for a little longer…
            </div>
          </Dialog>
          <Dialog
            actions={[
              <FlatButton onClick={this.actions.reset} style={{ color : '#8E312A', fontWeight : "bold", padding : '2px 5px' }}>Back to mission screen</FlatButton>
            ]}
            modal={false}
            open={this.state.cumulatedEnergy >= this.state.energyBeforeLose}
            onRequestClose={this.actions.reset}
            style={{ textAlign : 'center' }}
            bodyStyle={{ backgroundColor : '#CCC' }}
            actionsContainerStyle={{ backgroundColor : '#CCC' }}
          >
            <div style={{ color : '#8E312A' }}>
              <strong>The Star Killer just fired…<br/>
                Because of few, Billions of people are expected to die with their star system.</strong><br/><br/>
              More than ever, we must fight for human rights…
            </div>
          </Dialog>
          {this.state.screen === 'intro' && <Intro game={this.state} actions={this.actions}/>}
          {this.state.screen === 'game' && <Game game={this.state} actions={this.actions}/>}
        </div>
      </MuiThemeProvider>
    );
  }
}

export
default
App;
