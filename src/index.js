import 'babel-polyfill'

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

Object.defineProperty(Array.prototype, 'merge', {
  enumerable : true,
  value      : function(transform) {
    let counts = {};
    this.forEach(x => { counts[x] = (counts[x] || 0) + 1; });
    return Object.keys(counts).map(k => transform(k, counts[k]));
  }
})

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();

if (module.hot) {
  module.hot.accept('./App', () => {
    const NextApp = require('./App').default
    ReactDOM.render(
      <NextApp />,
      document.getElementById('root')
    )
  })
}
