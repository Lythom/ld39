import React, { Component } from 'react';
import './App.css';

import ButtonWithPopover from './ButtonWithPopover'

function getDescriptions(item) {
  if (item == null) return ''
  return <div>
    <ul>
      {item.bonusList
        .filter(b => b.dynamicDescription != null)
        .map((b, i) => <li key={i}>{b.dynamicDescription}</li>)
      }
    </ul>
  </div>
}

class Item extends Component {


  render() {
    return (
      <li>
        <ButtonWithPopover
          label={this.props.item.id}
          buttonClassName="bordered"
          buttonStyle={{ width : 180, color : 'white', textAlign : 'left', backgroundColor : 'rgb(48, 48, 48)', margin : 1, paddingLeft : 5 }}>
          <div style={{ padding: '0 10px' }}>
            {getDescriptions(this.props.item)}
          </div>
        </ButtonWithPopover>
      </li>
    );
  }
}

export default Item;

