/**
 * Autostuff
 *
 * # Summary
 *
 * Autostuff allows the developer to declare some :
 *
 * * attributeSet (ex: RPG style attributes in an object), and
 * * items with their bonus(es) (ex: a Sword that give flat 5 atk and +10% atk).
 *
 * Autostuff can then give you back :
 *
 * * the ready-to-use attributeSet with all calculated bonuses values, and
 * * an item tree with the calculated bonus values in each item (to know where values comes from)
 *
 * # Exemple
 *
 * TODO
 *
 * # Quick start
 *
 * 1. Create an attributeSet (an object with keys (attributes) and values (numbers, string, arrays, subObjects)
 * 2. Create items with id, and bonuses
 * An item can equipe items iteslef
 * A bonus is a bonusId, a priority, and a formula (function) which, given an attributeSet, can return one bonusValue object
 * An bonusValue is an internal object that can be applied on an attributeSet to get a new attributeSet with granted bonus
 * 3. const { calculatedAttributes, calculatedItems} = calculate(theHeroAttributes, theEquipedItems)
 *
 * # algorithm description
 *
 * ## Overview
 *
 * ### Calculate bonuses operations for each item and subitem
 *
 * Loop over items in the items array
 *  <- get existing priorities in items bonuses sorted
 *
 * // calculate, priority by priority, the bonusValue
 * Loop over all priorities
 *  -> Loop over items
 *    -> loop over bonuses
 *        <- calculate bonus values based on calculated attributeSet with previous priorities bonuses
 *      <- get list of bonuses updated with bonusValue for the items
 *    <- get updated list of items for the current priority
 *  <- get updated list of items
 *
 * ### Calculate final attributeSet by applying all those bonusesValue
 *
 * Apply the bonus value to the attributeSet recursively
 *
 * # Specifications
 *
 * ## AttributeSet
 *
 * AttributeSet must be an object, properties can be :
 *
 *  * nested objects (bonus is recursively merged into object),
 *  * numbers (bonus is added)
 *  * string (bonus is concatenated, order is not guaranteed !)
 *  * array (bonus is recursively merged, array is treated as an object)
 *  * undefined or null or NaN (if the attr key/value does not exist or is invalid, the bonus will override with its value)
 *
 *  Any other type in the attributeSet or in the bonusValue, that is used in an operation, will throw an error during calculation.
 *
 *  ## Item
 *
 *  Item must be an object {id, bonusList, equipedItems}
 *
 *  id should be unique within the item tree:
 *
 *  * this is not required for the calculations to run,
 *  * this is required for the itemTreeCrawler to work,
 *  * it might be required for your game logic to work (depends on what you do).
 *
 *  bonusList must be an array (even empty)
 *  equipedItem must be an array (even empty)
 *
 *  The Item(id, [bonusList], [equipedItems]) function create this object
 *
 *  Additional custom properties can be added to the item, they will be kept in the calculatedItems result, in the parameters given to bonus, and in the itemTreeCrawler functions.
 *  This allows to write complex bonus calculation behaviours based on holder or other items custom properties.
 *
 *  ## Bonus
 *
 *  Bonus must be an object {bonusId, formula, description, priority}
 *
 *  * bonusId is not as needed as itemId, but might be handy for debugging the game logic.
 *  * formula {formula} will compute the calculation of a "calculatedValue" property. This calculated value is used as an AttributeSet for equipedItems.
 *  * description {description} will compute the calculate of a "dynamicDescription" property. ie. This can be used to output dynamic labels for the user.
 *  * priority {number} among other items and bonuses based on the same AttributeSet, bonusValues from bonus of lower priority will be taken into account to calculate
 *  bonus of higher priority. ie. a +10% bonus of priority 1 will include the calculated bonus of priority 0 in the 10%, but not the calculated bonus or priority 1 or more.
 *
 */

/**
 * Calculate final attributes from an attributeSet attributes and items
 * @param {AttributeSet} attributes
 * @param {Array.<Item>} items
 * @return {Object}                         Calculated Attributes and items
 * @return {Object.calculatedAttributes}    Calculated Attributes
 * @return {Object.calculatedItems}         Items with calculatedBonus
 */
export function calculate(attributes, items) {

    if (!attributes || !Array.isArray(items) || items.length === 0) return {
        calculatedAttributes : attributes,
        calculatedItems      : items
    }

    const calculatedItemsAndSubItems = calculateItemsAndSubItemsBonusesValues(attributes, items, items)
    const calculatedAttributes = calculateAttributes(attributes, calculatedItemsAndSubItems)

    return {
        calculatedAttributes : calculatedAttributes,
        calculatedItems      : calculatedItemsAndSubItems
    }
}

/**
 * When all items and subitems are calculated, we can calculate the whole bonus
 * @param attributes
 * @param calculatedItems
 * @returns {*}
 */
function calculateAttributes(attributes, calculatedItems) {
    if (!Array.isArray(calculatedItems) || calculatedItems.length === 0) return attributes

    const bonusesValue = getRecursiveTotalBonusesValue(calculatedItems)
    return applyBonusValue(
        attributes,
        bonusesValue
    )
}

/**
 * Recursilvely calculate the bonus values
 * @param attributes
 * @param items
 * @param rootItems
 * @returns {Array}
 */
function calculateItemsAndSubItemsBonusesValues(attributes, items, rootItems) {

    // calculate the bonus this items would grant on this attributeSet
    const calculatedItems = calculateItemsBonusesValue(attributes, items, rootItems)
    const bonusesValue = getTotalBonusesValue(calculatedItems)

    // the subItems applies on the combinedBonusesValue of the item. Now that each item is calculated, subItems can be calculated as well.
    const calculatedItemsAndSubItems = calculatedItems.map(item => Object.assign({}, item, {
        equipedItems : calculateItemsAndSubItemsBonusesValues(bonusesValue, item.equipedItems, rootItems)
    }))

    return calculatedItemsAndSubItems
}

/**
 * Items end with all bonuses having a "calculatedValue" that takes into account all bonuses calculated by priority from attributes.
 * @param attributes
 * @param items
 * @param rootItems
 * @returns {Array.<Item>} Items with calculatedBonuses
 */
function calculateItemsBonusesValue(attributes, items, rootItems) {

    const allBonuses = [].concat(...items.map(item => item.bonusList))

    const priorities = allBonuses.reduce((priorities, bonus) => {
        if (priorities.indexOf(bonus.priority) === -1) priorities.push(bonus.priority)
        return priorities
    }, []).sort((a, b) => a - b)

    // calculate bonuses by priority
    const itemsWithCalculatedBonusValues = priorities.reduce((cumulatedItems, priority) => {

        // apply on attributes the cumulated bonus value in order to calculate next bonuses
        const cumulatedBonusesValue = getTotalBonusesValue(cumulatedItems)
        const cumulatedAttr = applyBonusValue(attributes, cumulatedBonusesValue)

        // for each item, calculate the bonus granted cumulated by priority
        const itemsWithCumulatedBonus = cumulatedItems.map(item => {
            return calculateItemBonuses(item, cumulatedAttr, priority, rootItems)
        })

        return itemsWithCumulatedBonus
    }, items)

    return itemsWithCalculatedBonusValues

}

function getTotalBonusesValue(items) {
    return applyBonusesValue({}, [].concat(...items.map(item => item.bonusList.map(b => b.calculatedValue))))
}

function getRecursiveTotalBonusesValue(items) {
    const localValue = getTotalBonusesValue(items)
    const subValue = applyBonusesValue(localValue, items.map(item => getRecursiveTotalBonusesValue(item.equipedItems)))
    return subValue
}

function calculateItemBonuses(item, cumulatedAttr, priority, rootItems) {
    const calculatedBonusesForPriority = calculateBonusesValuesOfOneItemPriority(item, cumulatedAttr, priority, rootItems)
    return Object.assign({}, item, {
        // update the bonuses of the current priority, the other remain unchanged
        bonusList : [...item.bonusList.filter(bonus => bonus.priority !== priority), ...calculatedBonusesForPriority]
    })
}

/**
 * @param item
 * @param attributes
 * @param priority
 * @param rootItems
 * @returns {Item} the item with the calculated bonuses
 */
function calculateBonusesValuesOfOneItemPriority(item, attributes, priority, rootItems) {
    const value = item.bonusList
        .filter(bonus => bonus.priority === priority)
        .map(bonus => {
            const calculatedValue = bonus.formula(attributes, item, rootItems)
            return Object.assign({}, bonus, {
                calculatedValue,
                dynamicDescription : typeof bonus.description === 'function' ? bonus.description(calculatedValue, attributes, item, rootItems) : bonus.description,
            })
        })
    return value
}

/**
 * Combine bonuses values use attributes as base
 * @param attributes
 * @param bonusesValues
 */
export function applyBonusesValue(attributes, bonusesValues) {
    return bonusesValues.reduce((cumulatedAttributes, bonusValue) => {
        return applyBonusValue(cumulatedAttributes, bonusValue)
    }, attributes)
}

export function applyBonusValue(attributes, bonusValue) {

    if (attributes == null) attributes = Array.isArray(bonusValue) ? [] : {}

    if (typeof attributes !== 'object') throw new Error('Can\'t apply a bonusValue on a non-object')
    if (bonusValue == null) return attributes
    if (typeof bonusValue !== 'object') throw new Error('Can\'t merge non-object bonusValue in an object')

    let newAttributes = Array.isArray(attributes) ? [...attributes] : Object.assign({}, attributes)

    return Object.keys(bonusValue).reduce((calculatedAttributes, key) => {
        const attributeValue = attributes[key]
        const bValue = bonusValue[key]
        const operation = getOperation(attributeValue) || getOperation(bValue)
        calculatedAttributes[key] = applyValue(attributeValue, bValue, operation)
        return calculatedAttributes;
    }, newAttributes)
}

/**
 * Apply bonusValue on attribute using specified operation.
 * attribute and bonusValue are permutable for operations add and merge : applyValue(a, b, 'add') == applyValue(b, a, 'add')
 * concat operation does not guarantee the output order since operation is not permutable : applyValue(a, b, 'concat') ~! applyValue(b, a, 'concat')
 * @param attribute
 * @param bonusValue
 * @param operation
 * @returns {*}
 */
export function applyValue(attribute, bonusValue, operation) {
    const attrType = typeof attribute
    const bonusValueType = typeof bonusValue
    if (attrType != bonusValueType && !(attribute == null || bonusValue == null)) throw new Error(`Attribute (${String(attribute)}) and value (${String(bonusValue)}) are not the same type. Please check that the bonus return a value compatible with the attribute.`)
    switch (operation) {
        case "add" :
            return ((Number.isNaN(attribute) || attribute == null) ? 0 : attribute) + ((Number.isNaN(bonusValue) || bonusValue == null) ? 0 : bonusValue)
        case "merge" :
            return applyBonusValue(attribute, bonusValue)
        case "concat" :
            return (attribute == null ? '' : attribute).concat(bonusValue == null ? '' : bonusValue)
        default:
            throw new Error('Operation is not supported : ' + operation)
    }
}

function getOperation(attributeValue) {
    switch (typeof attributeValue) {
        case 'number' :
            return 'add'
        case 'string' :
            return 'concat'
        // true for plain object and arrays
        case 'object' :
            return 'merge'
        case 'undefined' :
            return null
    }
}

/**
 *
 * @param {String} bonusId          Used to document the bonus behaviour.
 * @param {formula} formula         AttributeSet->Bonus->BonusValue
 * @param {description} description used to produce the dynamicDescription.
 * @param {number} priority
 * @returns {Bonus} A Bonus object. The Bonus formula is used in the calculation of BonusTransfom and calculatedValues from attributes.
 * @constructor
 */
export function Bonus(bonusId, formula, description = null, priority = 0)
{
    if (process.env.NODE_ENV !== 'production' && typeof formula !== 'function') throw new Error('formula should be a function (attrSet, equipedOn, items) => Object')
    if (process.env.NODE_ENV !== 'production' && !Number.isInteger(priority)) throw new Error('priority should be an integer')

    return {
        bonusId,
        formula,
        description,
        priority,
        calculatedValue : {}
    }
}

/**
 * Create a new Item
 * @param {String}              id                      unique id of the item, used to equip / unequip this Item from an Item holder
 * @param {Array.<Bonus>}       bonusList               List of Bonus used to generates the bonusesValues from the holder baseAttributes
 * @param {*}                   equipedItems            List of items equiped on top of this one. Those items uses "bonusesValues" as an attributeSet to apply other bonuses on top of it.
 * @returns {Item} Minimal state of an item
 */
export function Item(id, bonusList = [], equipedItems = []) {
    if (process.env.NODE_ENV !== 'production' && !Array.isArray(bonusList)) throw new Error('bonusList should be an array')
    if (process.env.NODE_ENV !== 'production' && !Array.isArray(equipedItems)) throw new Error('equipedItems should be an array')
    return {
        id           : id,
        bonusList    : bonusList,
        equipedItems : equipedItems,
    }
}

/**
 * An Object that contains attributes as keys.
 * Can be nested.
 * @name AttributeSet
 * @type {*}
 */

/**
 * Generates a BonusValue from an attributes list.
 * A BonusValue can be applied on top of an attributeSet to obtain calculated values.
 * A BonusValue itself can be considered as an attributeSet.
 @name formula
 @function
 @param {*} attributes. Attriutes the bonus should base its calculations on (parent attributes).
 @param {Item} holder. the item holding this bonus
 @param {Array.<Item>} rootItems. the root items applied on the original attributeSet
 @return {*} a bonus transform that acts as "diff" from attributes to calculatedAttributes.
 */

/**
 * Generates a dynamicDescription
 * @name description
 * @function
 * @param {*} calculatedBonus.          The BonusValue that this bonus currently grants
 * @param {*} attributes.               Attriutes the bonus should base its calculations on (parent attributes)
 * @param {Item} holder.                The item holding this bonus.
 * @param {Array.<Item>} rootItems.     the root items applied on the original attributeSet.
 * @return {*} a bonus transform that acts as "diff" from attributes to calculatedAttributes.
 */

/**
 * Bonus
 * @name Bonus
 * @property {String}               bonusId             Used to identify the bonus
 * @property {formula}              formula             AttributeSet->Item->rootItems->BonusValue
 * @property {description|string}   description         calculatedValue->AttributeSet->Item->rootItems->Description. Return a description that can be used to document the current state of the bonus
 * @property {number}               priority            Among the siblings bonuses, lower priority bonus are taken into account to calculate the higher priority bonus
 * @property {Object}               calculatedValue     A bonus transform that acts as "diff" from attributes to calculatedAttributes.
 */

/**
 * Item
 * @name Item
 * @property {String}           id                    unique id of the item, used to equip / unequip this Item from an Item holder, and to use the itemTreeCrawler functions.
 * @property {Array.<Bonus>}    bonusList             List of Bonus used to generates the bonusesValues from the holder baseAttributes
 * @property {Array.<Item>}     equipedItems          List of items equiped on top of this one. Those items uses "calculatedBonus" as baseAttributes to apply other bonuses on top of it.
 */

/**
 * Any Object that can equip items.
 * Ex: Item themsleves can act as ItemHolder.
 * @name ItemHolder
 */

/**
 * return a new objct with Equipped item (Assume immutable)
 * @param {ItemHolder} holder to unequip
 * @param {Array.<Item>} items
 * @returns {ItemHolder} new AttributeSet with added item
 */
export function equip(holder, ...items) {
    return Object.assign({}, holder, {
        equipedItems : [...holder.equipedItems, ...items]
    })
}

/**
 * return a new objct with Unequiped item (Assume immutable)
 * @param {ItemHolder} holder to unequip
 * @param {Item} item to unequip
 * @returns {ItemHolder} new Item with removed sub item
 */
export function unequip(holder, item) {
    const index = holder.equipedItems.findIndex(i => i.id === item.id)
    if (index < 0) {
        return this
    }
    return Object.assign({}, holder, {
        equipedItems : holder.equipedItems.filter(i => i.id !== item.id)
    })
}


// Un bonus value doit pouvoir s'additionner avec un autre bonus value
// mergedBonusesValue = addBonusesValue(bonusesValue)

// Des bonus doivent pouvoir s'appliquer à des attributes
// newAttributes = applyBonuses(attributes, bonusList)

// Un bonus value doit pouvoir servir de référence de la même façon qu'un attribut pour recevoir lui même des opérations de bonus
// bonusValue = applyBonusesValue(attributes, bonuses)
// HigherBonusValue = applyBonusValue(bonusValue, higherBonus)
