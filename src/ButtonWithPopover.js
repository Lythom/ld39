import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import './App.css';
import FlatButton from 'material-ui/FlatButton';
import { compose, withState, withHandlers } from 'recompose'

import { Popover } from 'material-ui'

const withToggle = compose(
  withState('toggledOn', 'toggle', false),
  withHandlers({
    show   : ({ toggle }) => (e) => toggle(true),
    hide   : ({ toggle }) => (e) => toggle(false),
    toggle : ({ toggle }) => (e) => toggle((current) => !current)
  })
)

// props: label, buttonClassName, buttonStyle children
class ButtonWithPopover extends Component {

  constructor() {
    super()
    this.state = {
      anchorEl : null
    }
  }

  registerAnchorEl = (anch) => {
    this.setState({
      anchorEl : findDOMNode(anch)
    })
  }

  render() {
    return (
      <div className={this.props.className}>
        <FlatButton
          ref={this.registerAnchorEl}
          onClick={this.props.toggle}
          className={this.props.buttonClassName}
          style={this.props.buttonStyle}
        >
          {this.props.label}
        </FlatButton>
        <Popover
          open={this.props.toggledOn}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal : 'right', vertical : 'top' }}
          targetOrigin={{ horizontal : 'left', vertical : 'top' }}
          onRequestClose={this.props.hide}
          useLayerForClickAway={false}
        >
          {this.props.children}
        </Popover>
      </div>
    );
  }
}

export default withToggle(ButtonWithPopover);

