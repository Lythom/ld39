import React, { Component } from 'react';
import './App.css';
import PowerStation from './PowerStation'

class PowerStations extends Component {
  render() {
    return (
      <div className="Cards">
        {this.props.game.powerStations && Object.keys(this.props.game.powerStations).map((ps, i) => (
          <div key={i} className="powerstation" style={{ transform : `translate(${i * 210 + 5}px, 5px)` }}>
            <PowerStation game={this.props.game} id={ps} actions={this.props.actions} />
          </div>
        ))}
      </div>
    );
  }
}

export default PowerStations;

