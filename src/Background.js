import React, { Component } from 'react';
import './App.css';
import gear1 from './images/gear1.svg';
import gear2 from './images/gear2.svg';
import bg from './images/bg.png';

class Background extends Component {

  constructor() {
    super()
    this.state = {
      rotation: 0,
    }
  }

  componentDidMount() {
    this.rotate = setInterval(() => {
      this.props.game && this.setState(prevState => ({rotation: (prevState.rotation + this.props.game.speed)%360}))
    }, 1000/60)

  }

  componentWillUnmount() {
   clearInterval(this.rotate)
  }
  render() {
    return (
      <div className="Background">
        <img className="abs" src={gear1} alt="" style={{ transform : `translate(620px, 50px) rotate(${this.state.rotation}deg)` }}/>
        <img className="abs" src={gear2} alt="" style={{ transform : `translate(590px, 110px) rotate(${this.state.rotation * 2}deg)` }}/>
        <img className="abs" src={bg} alt="" />
      </div>
    );
  }
}

export default Background;
