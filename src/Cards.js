import React, { Component } from 'react';
import './App.css';
import { Card, CardActions, CardMedia, CardTitle } from 'material-ui/Card'
import QuoteIcon from 'material-ui/svg-icons/editor/format-quote'
import { calculate } from './autostuff'

function getDescriptions(item) {
  if (item == null) return ''
  const { calculatedItems, calculatedAttributes } = calculate({}, [item])
  return <ul>{calculatedItems
    .map(item => item.bonusList
      .map(b => <li>{b.bonusId}</li>)
    )
  }</ul>
}

class Cards extends Component {
  render() {
    return (
      <div className="Cards">
        {this.props.actions.getCurrentCards().filter(c => c != null).map((c, i) => (
          <div key={i} className="card" style={{ transform : `translate(650px, 235px)` }}>
            <Card>
              <CardTitle style={{ textAlign : 'left' }} title={c.id}/>
              {c.img && <CardMedia><img src={c.img} alt=""/></CardMedia>}
              <CardActions style={{ fontSize : '0.8rem', color : 'lightgray' }}>{c.quote && (
                <div>
                  <div style={{ textAlign : 'left' }}>
                    {getDescriptions(c)}
                  </div>
                  <QuoteIcon style={{ width : 13, height : 13, transform : 'rotate(180deg)', color : 'lightgray' }}/>
                  {c.quote}
                  <QuoteIcon style={{ width : 13, height : 13, color : 'lightgray' }}/>
                </div>)
              }
              </CardActions>
            </Card>
          </div>
        ))}
      </div>
    );
  }
}

export default Cards;

