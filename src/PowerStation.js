import React, { Component } from 'react';
import './App.css';
import { Card, CardActions, CardMedia, CardTitle } from 'material-ui/Card'
import FlatButton from 'material-ui/FlatButton';

import manpower from './images/manpower.png'
import efficiency from './images/efficiency.png'
import sendhere from './images/sendhere.png'

import { powerStations, getCardById, calculateOutput } from './data'
import { calculate } from './autostuff'
import Item from './Item'
import ButtonWithPopover from './ButtonWithPopover'

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const Effect = ({ attr }) => (
  <ButtonWithPopover
    label={attr}
    className="effect"
    buttonStyle={{
      color           : 'white',
      textAlign       : 'center',
      backgroundColor : 'rgb(48, 48, 48)',
      margin          : 1,
      padding         : 0,
      display         : 'inline-block',
      border          : 0,
      minWidth        : null,
      height          : null,
      lineHeight      : null,
    }}
  >
    <div style={{ padding : '10px 10px' }}>
      This power station has "{attr}". <br/>It can triggers some effects on cards.
    </div>
  </ButtonWithPopover>
)

const Attribute = ({ img, attrValue, attrName, calculatedItems }) => (
  <ButtonWithPopover
    label={<span><img src={img} alt={attrName}/> <strong>{attrValue}</strong></span>}
    buttonStyle={{
      color      : 'white',
      textAlign  : 'center',
      minWidth   : null,
      height     : null,
      lineHeight : null,
    }}
  >
    <div style={{ padding : '10px 10px' }}>
      <div>
        {capitalizeFirstLetter(attrName)}: <strong>{attrValue}</strong>
      </div>
      <div>
        {calculatedItems.length === 0 ? <span>No active modifier</span> : <span>Active modifiers :</span>}
        <ul>
          {[].concat(...calculatedItems.map(item => (
              item.bonusList
                .filter(b => Object.keys(b.calculatedValue).some(attr => attr === attrName && b.calculatedValue[attr] !== 0))
                .map(b => b.dynamicDescription + ' from ' + item.id)
            )
          )).merge((value, count) => count === 1 ? value : `${count}x ${value}`)
            .map(desc => <li key={desc}>{desc}</li>)}
        </ul>
      </div>
    </div>
  </ButtonWithPopover>
)

function getDescriptions(powerStation) {
  if (powerStation == null) return ''
  let { calculatedAttributes, calculatedItems } = powerStation
  if (calculatedAttributes == null) calculatedAttributes = powerStation.attributes
  if (calculatedItems == null) calculatedItems = []
  return <div>
    <div className="effects">
      {Object.keys(calculatedAttributes)
        .filter(attr => attr !== 'manpower' && attr !== 'efficiency')
        .map((eff, i) => <Effect key={i} attr={eff}/>)}
    </div>
    <ul className="list" style={{ display : 'inline-block', width : '50%' }}>
      <li>
        <Attribute img={manpower} attrName="manpower" attrValue={Math.max(0, calculatedAttributes.manpower)} calculatedItems={calculatedItems}/>
      </li>
      <li>
        <Attribute img={efficiency} attrName="efficiency" attrValue={Math.max(0, calculatedAttributes.efficiency)} calculatedItems={calculatedItems}/>
      </li>
    </ul>
    <ul className="list" style={{ display : 'inline-block', width : '50%' }}>
      <li>&nbsp;+<strong>{calculateOutput(calculatedAttributes)} &#x26a1;</strong> per&nbsp;turn</li>
    </ul>
  </div>
}

class PowerStation extends Component {

  placeCard = () => {
    const { id, game } = this.props
    this.props.actions.addItem(id, game.currentCard)
    this.props.actions.pickNewCard()
  }

  render() {
    const { id, game } = this.props
    const data = game.powerStations[id]
    return (
      <div>
        {data.img && <CardMedia><img src={data.img} alt=""/></CardMedia>}
        <div>
          {getDescriptions(data)}
          <FlatButton
            onClick={this.placeCard}
            className="bordered"
            style={{ color : '#8E312A', backgroundColor : '#DDD', fontWeight : 'bold', margin : '5px 0 15px 0', padding : '0 5px' }}>
            <img src={sendhere} alt="Send here"/>
          </FlatButton>
          <ul className="list">{data.calculatedItems && data.calculatedItems.map((item, idx) => (
            <Item key={idx} item={item}/>
          ))}</ul>
        </div>
      </div>
    );
  }
}

export default PowerStation;

