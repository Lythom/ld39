/**
 * Get the parent item of itemId in the items tree.
 * @param {Array} items
 * @param {String} itemId
 * @returns {*|null} parent item
 */
export function getParent(items, itemId) {
    if (!Array.isArray(items) || items.length === 0 || itemId == null) return null
    const parent = items.find(child => child.equipedItems.some(i => i.id === itemId))
    if (parent != null) return parent
    for (let i = 0; i < items.length; i++) {
        const p = getParent(items[i].equipedItems, itemId);
        if (p != null) return p
    }
    return null
}

/**
 * Get the siblings itemId in the items tree.
 * The item itslef is included with the siblings.
 * @param {Array} items
 * @param {String} itemId
 * @returns {Array|null} List of the siblings, including the item
 */
export function getSiblings(items, itemId) {
    if (items == null || itemId == null) return null
    if (items.some(i => i.id === itemId)) {
        return items
    }
    for (let i = 0; i < items.length; i++) {
        const s = getSiblings(items[i].equipedItems, itemId);
        if (s != null) return s
    }
    return null
}
