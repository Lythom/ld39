import React, { Component } from 'react';
import logo from './logo.svg';
import title from './images/title.png';
import './App.css';

class Intro extends Component {

  constructor() {
    super()
    this.gotoGame = this.gotoGame.bind(this)
  }

  componentDidMount() {
    window.addEventListener('click', this.gotoGame)
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.gotoGame)
  }

  gotoGame() {
    this.props.actions.goto('game')
  }

  render() {
    return (
      <div>
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <h2><img src={title} alt="Sabotage"/></h2>
        </div>
        <p className="App-intro">
          <p>
            <strong>Proxima Solar system in endangered !</strong><br/>
            The Star Killer is about to fire. It's our last chance to prevent the destruction of a solar system and it's billions inhabitants.
          </p>
          <p>
            <strong>Our plan ?</strong><br/> Attacking the Power Stations.<br />If the Star killer run out of power, it won't do any harm. <br/>
            By cumulating enough malus on stations, we can do it !
          </p>
          <p>
            <br/>
            <strong>Click anywhere to start the mission</strong>
          </p>
        </p>
        <div style={{ position : 'absolute', bottom : 10, right: 10 }}>By Lythom - For Ludum Dare 29 (theme : Running out of power)</div>
      </div>
    );
  }
}

export default Intro;
